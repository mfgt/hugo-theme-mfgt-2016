'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var googleWebFonts = require('gulp-google-webfonts');

gulp.task('scripts', function() {
    gulp.src([
            'static-src/js/**/*.js',
            './node_modules/jquery/dist/jquery.js',
            './node_modules/fancybox/dist/js/jquery.fancybox.js',
            './node_modules/jquery-fancybox/lib/jquery.mousewheel-3.0.6.pack.js',
        ])
        .pipe(gulp.dest('static/js'));
});

gulp.task('css', function() {
    gulp.src([
            'static-src/css/**/*.css',
            './node_modules/fancybox/dist/css/jquery.fancybox.css',
            './node_modules/font-awesome/css/font-awesome.min.css',
        ])
        .pipe(gulp.dest('static/css'));
});

gulp.task('fonts', function() {
    gulp.src([
            './node_modules/font-awesome/fonts/*',
        ])
        .pipe(gulp.dest('static/fonts'));
});

gulp.task('gfonts', function() {
    var options = {
        fontsDir: '../fonts/',
        cssDir: '',
        cssFilename: 'fonts.css'
    };
    gulp.src('./fonts.list')
        .pipe(googleWebFonts(options))
        .pipe(gulp.dest('static/css'));
});

gulp.task('images', function() {
    gulp.src([
            './node_modules/fancybox/dist/img/*',
        ])
        .pipe(gulp.dest('static/img'));
});

gulp.task('default', function() {
    gulp.run('scripts', 'css', 'fonts', 'gfonts', 'images');
    gulp.watch('static-src/js/**', function(event) {
        gulp.run('scripts');
    });

    gulp.watch('static-src/css/**', function(event) {
        gulp.run('css');
    });

    gulp.watch('static-src/fonts/**', function(event) {
        gulp.run('fonts');
    });

    gulp.watch('static-src/img/**', function(event) {
        gulp.run('images');
    });
})
