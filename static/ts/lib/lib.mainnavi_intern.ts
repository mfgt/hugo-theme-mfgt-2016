## MAIN DROPDOWN NAVIGATION
lib.field_mainnavi = HMENU
#lib.field_mainnavi.entryLevel = 0
lib.field_mainnavi.excludeUidList = 28,29

# 1.Level
lib.field_mainnavi.1 = TMENU
lib.field_mainnavi.1{
  noBlur=1
  wrap= <ul id="menubar">|</ul>
  expAll = 1
    NO{
        wrapItemAndSub = <li class="main-nav-first-child nav-item">|</li> |*| <li class="nav-item">|</li> |*| <li class="main-nav-last-child nav-item">|</li>
        stdWrap.wrap = <span>|</span>
    }
    ACT = 1
    ACT.ATagParams = class="main-nav-1-act"
    ACT.stdWrap.wrap =
    ACT {
        allWrap=<li>|</li>
        stdWrap.wrap = <span>|</span>
    }
    ACTIFSUB = 1
    ACTIFSUB.ATagParams = class="main-nav-1-act"
    ACTIFSUB.stdWrap.wrap =
    ACTIFSUB {
        wrapItemAndSub = <li class="main-nav-first-child nav-item">|</li>  |*| <li class="nav-item">| |*| <li class="main-nav-last-child nav-item">|
        stdWrap.wrap = <span>|</span>
    }
    IFSUB = 1
    IFSUB.stdWrap.wrap =
    IFSUB {
        wrapItemAndSub =<li class="main-nav-first-child nav-item">|</li>   |*| <li class="nav-item">| |*| <li class="main-nav-last-child nav-item">|
    stdWrap.wrap = <span>|</span>
    }
}


# 2.Level
lib.field_mainnavi.2 = TMENU
lib.field_mainnavi.2 {
noBlur=1
expAll = 1
wrap =<ul class="nav-submenu">|</ul></li>
    NO{
         linkWrap= <li>|</li>
         allStdWrap.insertData = 1   
         stdWrap.wrap = <span class="fa {field:author} fa-fw fa-color-1"></span><span>|</span>
      }
    ACT < .NO
    ACT = 1
    ACT.ATagParams = class="main-nav-2-act"
    ACT{
        linkWrap= <li>|</li>
        allStdWrap.insertData = 1  
        stdWrap.wrap = <span class="fa {field:author} fa-fw fa-color-4"></span><span>|</span>
        doNotLinkIt = 0
    }
    ACTIFSUB = 1
    ACTIFSUB.ATagParams = class="main-nav-2-act"
    ACTIFSUB.stdWrap.wrap =
    ACTIFSUB {
        linkWrap = <li>|
        allStdWrap.insertData = 1  
        stdWrap.wrap =  <span class="fa {field:author} fa-fw fa-color-4"></span><span>|</span>
    }
    IFSUB = 1
    IFSUB.stdWrap.wrap =
    IFSUB {
        linkWrap = <li>|
        allStdWrap.insertData = 1  
        stdWrap.wrap =  <span class="fa {field:author} fa-fw fa-color-1"></span><span>|</span>
    }
}

# 3.Level
lib.field_mainnavi.3 = TMENU
lib.field_mainnavi.3 {
noBlur=1
expAll = 1
wrap =<ul>|</ul></li>
    NO{
         linkWrap= <li>|</li>
         stdWrap.wrap = <span class="fa fa-angle-right fa-fw fa-color-1"></span><span>|</span>
      }
    ACT < .NO
    ACT = 1
    ACT.ATagParams = class="main-nav-3-act"
    ACT{
        linkWrap= <li>|</li>
        stdWrap.wrap = <span class="fa fa-angle-right fa-fw fa-color-4"></span><span>|</span>
        doNotLinkIt = 0
    }
    ACTIFSUB = 1
    ACTIFSUB.ATagParams = class="main-nav-3-act"
    ACTIFSUB.stdWrap.wrap =
    ACTIFSUB {
        linkWrap= <li>|</li>
        stdWrap.wrap = <span class="fa fa-angle-right fa-fw fa-color-4"></span><span>|</span>
    }
    IFSUB = 1
    IFSUB.stdWrap.wrap =
    IFSUB {
        linkWrap= <li>|</li>
        stdWrap.wrap = <span class="fa fa-angle-right fa-fw fa-color-1"></span><span>|</span>
    }
}
