# *****************
# CType: bullet
# *****************
tt_content.bullets = COA
tt_content.bullets {
	10 = < lib.stdheader

	20 = TEXT
	20 {
		field = bodytext
		trim = 1
		split{
			token.char = 10
			cObjNum = |*|1|| 2|*|
			1.current = 1
			1.parseFunc =< lib.parseFunc
			1.wrap = <li><i class="fa-li fa fa-circle color-10"></i>|</li>

			2.current = 1
			2.parseFunc =< lib.parseFunc
			2.wrap = <li><i class="fa-li fa fa-circle color-10"></i>|</li>
		}
		dataWrap = <ul class="fa-ul csc-bulletlist-{field:layout}">|</ul>
	 	editIcons = tt_content: bodytext, [layout]
	 	editIcons.beforeLastTag = 1
	 	editIcons.iconTitle.data = LLL:EXT:css_styled_content/pi1/locallang.php:eIcon.bullets

	 	prefixComment = 2 | Bullet list:
	}
}




### Set default class for ul from rte
lib.parseFunc_RTE {
  tags.typolist.split.wrap = <li>test|</li>
  tags.typolist.stripNL = 1
  externalBlocks := addToList(ul,li)
  externalBlocks {
    ul.stripNL = 1
    ul.callRecursive = 1
    ul.callRecursive.tagStdWrap.HTMLparser = 1    
    ul.callRecursive.tagStdWrap.HTMLparser.tags.ul {
      fixAttrib.class.default = fa-ul ul-rte
    }
  }
}