## Left Navi
lib.field_leftnavi= HMENU
lib.field_leftnavi.entryLevel = 1

lib.field_leftnavi.1 = TMENU
lib.field_leftnavi.1{
wrap =  <ul id="left-navi">|</ul>
    NO{
        wrapItemAndSub = <li>|</li>
        stdWrap.wrap = <i class="fa fa-circle fa-color-1"></i><span>|</span>
  }
    ACT < .NO
    ACT = 1
    ACT{
        wrapItemAndSub = <li class="act-has-sub">|</li>
        stdWrap.wrap = <i class="fa fa-circle fa-color-1"></i><span>|</span>
        doNotLinkIt = 0
    }
    ACTIFSUB < .NO
    ACTIFSUB = 1
    ACTIFSUB{
        wrapItemAndSub = <li class="act-has-sub">|
        stdWrap.wrap = <i class="fa fa-circle fa-color-1"></i><span>|</span>
        doNotLinkIt = 0
    }
}
lib.field_leftnavi.2 = TMENU
lib.field_leftnavi.2{
expAll = 1
wrap = <ul>|</ul></li>
    NO{
        wrapItemAndSub = <li>|</li>
        stdWrap.wrap = <i class="fa fa-angle-right fa-fw fa-color-1"></i><span>|</span>
  }
    ACT < .NO
    ACT = 1
    ACT{
        wrapItemAndSub = <li class="sub-act">|</li>
        stdWrap.wrap = <i class="fa fa-angle-right fa-fw fa-color-1"></i><span>|</span>
        doNotLinkIt = 0
    }
    ACTIFSUB < .NO
    ACTIFSUB = 1
    ACTIFSUB{
        wrapItemAndSub = <li  class="sub-act">|</li>
        stdWrap.wrap = <i class="fa fa-angle-right fa-fw fa-color-1"></i><span>|</span>
        doNotLinkIt = 0
    }
    IFSUB < .NO
    IFSUB = 1
    IFSUB{
        wrapItemAndSub = <li>|</li>
        stdWrap.wrap = <i class="fa fa-angle-right fa-fw fa-color-1"></i><span>|</span>
        doNotLinkIt = 0
    } 
}