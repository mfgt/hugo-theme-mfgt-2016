/* 
 *  @author (c) 2014  Aji yahya <aji.yahya@gmail.com>
 *  All rights reserved
 */

jQuery.noConflict();
(function($) { 


function cycleImages(){
      var $active = $('#header-image-rotator .rotatoractive');
      var $next = ($active.next().length > 0) ? $active.next() : $('#header-image-rotator img:first');
      $next.css('z-index',2);//move the next image up the pile
      $active.fadeOut(1500,function(){//fade out the top image
	      $active.css('z-index',1).show().removeClass('rotatoractive');//reset the z-index and unhide the image
          $next.css('z-index',3).addClass('rotatoractive');//make the next image the top one
      });
    }

$(document).ready(function(){
// run every 4s
setInterval(cycleImages, 10000);
})

;})(jQuery);