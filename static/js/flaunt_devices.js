/*
	Flaunt.js v1.0.0
	by Todd Motto: http://www.toddmotto.com
	Latest version: https://github.com/toddmotto/flaunt-js
	
	Copyright 2013 Todd Motto
	Licensed under the MIT license
	http://www.opensource.org/licenses/mit-license.php

	Flaunt JS, stylish responsive navigations with nested click to reveal.
*/
(function($) {
        // DOM ready
	$(function() {
           
		$('.nav-item').has('ul').prepend('<span class="nav-click"><span class="nav-arrow fa touch-fa-chevron-down fa-fw fa-color-1 pull-right"></span></span>');
                $('#menubar').on('click', '.nav-click', function(event){
                    
                    $('.nav-submenu').not($(this)).slideUp();
                    $(this).siblings('.nav-submenu').toggle();
                    if ($(this).children('.nav-arrow').hasClass('nav-rotate')) {
                        $(this).children('.nav-arrow').removeClass('nav-rotate');
                    }else{
                        $('.nav-arrow').removeClass('nav-rotate');
                        $(this).children('.nav-arrow').addClass('nav-rotate'); 
                    }
                if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {           
                    window.scrollTo(0,0);
                }else{
                    $('html,body').animate({  scrollTop: 0, scrollLeft: 0
                    }, 800, function(){
                        $('html,body').clearQueue();
                    });
                }
                
              
		event.preventDefault();
		});
        });
})(jQuery);