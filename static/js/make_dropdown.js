(function($){

  var timeout    = 500;
  var closetimer = 0;
  var ddmenuitem = null;
  var ddmenuitemClicked = '';
  var URL = '';

  // From https://github.com/Modernizr/Modernizr/blob/master/modernizr.js
  var isTouch = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

  // jsddm funcs from http://javascript-array.com/scripts/jquery_simple_drop_down_menu/
  function jsddm_open($this){
    jsddm_canceltimer();
    jsddm_close();
    $this.addClass('itemClicked');
    ddmenuitem = $this.find('ul').css({'visibility':'visible','display':'block'});
    
   // ddmenuitemClicked = $('li').hasClass('itemClicked');
  }
  function jsddm_openURL($this) {
      if(ddmenuitem) {
          URL = $(this).attr('href');
         //test alert (URL);
          window.location.href(URL);
      }
  }
  function jsddm_close() {
    if(ddmenuitem){
          ddmenuitem.css('visibility', 'hidden');
          ddmenuitem.css('display', 'none');
          ddmenuitem = null;
    }
  }

  function jsddm_timer() { 
    closetimer = window.setTimeout(jsddm_close, timeout);
  }

  function jsddm_canceltimer() { 
    if(closetimer)
    {  window.clearTimeout(closetimer);
       closetimer = null;
    }
  }
  function jsddm_toggle($this) {
    if (ddmenuitem && $this.has(ddmenuitem[0]).length){
      jsddm_close();
    }
    else {
      jsddm_open($this);  
    }
  }

  $.fn.make_dropdown = function(options){
      return this.each(function(){
      if (options && options['timeout']){
        timeout = options['timeout'];
      }

      $(this).on('click',function(event){
          var parent_li = $(this).parents('li');
          alert (parent_li);
       // alert ('reees');
       $(this).doubletap(function() {
          $('li').removeClass('itemClicked');
         //   jsddm_openURL();   
       });
     
       if(!$(this).hasClass('itemClicked')) {
          // event.preventDefault();
         // $("html, body").animate({ scrollTop: 0 }, "fast");  
       }else{
           $('li').removeClass('itemClicked');
           // jsddm_openURL();  
       }
        jsddm_toggle(parent_li);
       // event.stopPropagation();
        event.preventDefault();
      });
     // $(this).on('click touchstart',function(e) {
        //  e.preventDefault();
          
           //     $("html, body").animate({ scrollTop: 0 }, "fast");  
   //   });
      
      
     // if (!isTouch){
        //$(this).mouseover(function(){ jsddm_open($(this)) }).mouseout(jsddm_timer);
     // }

    });
  }
    if (isTouch){
        $(document).ready(function(){ $('html.touch span.navi-touch-dropdown').make_dropdown(); });
    }
    $(document).click(jsddm_close);

})(jQuery);
