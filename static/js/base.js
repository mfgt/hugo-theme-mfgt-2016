/* 
 *  @author (c) 2013  Aji yahya <aji.yahya@gmail.com>
 *  All rights reserved
 */

jQuery.noConflict();
(function($) { 
    $(document).ready(function()  {
     //   console.log();
     // alert ($(window).width());
     /* set header-image */
     //$('#header-image').css('height','1');
     var headerImgHeight = $('#header-image img').height();
     $('#header-image').css('height', headerImgHeight);
     // console.log(headerImgHeight);
        
            var     body 	= $('body'),
                    page 	= body.find('#main'),
                    navToggle 	= body.find('#nav-toggle');
            navToggle.on('click', function(){
              
                     // ie 7 8 9 fix
                    if (getInternetExplorerVersion() === 7 || getInternetExplorerVersion() === 8 || getInternetExplorerVersion() === 9) {
                        $('.cssmenu').animate({left:'-260px'});
                        $('#wrap').css('left','260px');
                    }
                    
                    body.removeClass('loading').toggleClass('nav-open');
                    
                   
                    if ( body.hasClass('nav-open') ) {
                             navToggle.addClass('navToggle');
                              $('.nav-open #menubar').css('height', '');
                              $('.nav-open #menubar').css('height', $(document).height());
                              $(window).resize(function () { 
                                  $('.nav-open #menubar').css('height', $(document).height());
                              });
                             
                    } else {
                              navToggle.removeClass('navToggle');
                              $('#menubar').css('height', '');
                              $(window).resize(function () { 
                                  $('#menubar').css('height', '');
                              });
                           
                            // ie 7 8 9 fix
                            if (getInternetExplorerVersion() === 7 || getInternetExplorerVersion() === 8 || getInternetExplorerVersion() === 9)  {
                                $('.cssmenu').animate({left:'-260px'});
                                $('#wrap').css('left','0');
                            }
                             
                    }
                    
            });
            
            $('.nav-open').on('click touchstart', function(){
                   removeClass('nav-open');
                   
             });
             
        
    
           
    });

    $(window).resize(function () { 
    // console.log($(window).width());
            /* set header-image */
            //$('#header-image').css('height', '');
      
            var headerImgHeight = $('#header-image img').height();
            $('#header-image').css('height', headerImgHeight);
           
            // console.log($(window).width());
            // remove style "display:none" from nav-submenu if window resized from Devices to Screen > 1024: 
            // Bug fix submenu was not showing after resize from DEvice to Screen Version
            if ($(window).width() > 1024) {
              //  $('.nav-submenu').css('display','');
                $('body').removeAttr('class');
               
            }
             $('#menubar').css('height', '');
          
    });
    
    
    
    /* Metanavi-home dropdown*/
    $(document).ready(function () {
    // SVG fix on IE8
    if (getInternetExplorerVersion() === 8) {$('img[src$=".svg"]').each(function(){$(this).attr('src', $(this).attr('src').replace('.svg', '.png'));});}
    
    var homenav = $('.dropdown-tool-menu');
    var timeout = 0;
    var hovering = false;
    homenav.hide();

    $('.dropdown-tool').on("mouseenter", function () {
        hovering = true;
        // Open the menu
        $('.dropdown-tool-menu')
            .stop(true, true)
            .slideDown(400);

        if (timeout > 0) {
            clearTimeout(timeout);
        }
    })
        .on("mouseleave", function () {
        resetHover();
    });
    
    $('#main').on('click' , function () {$(".dropdown-tool-menu").hide()}) ;
    $(".dropdown-tool-menu").on("mouseenter", function () {
        // reset flag
        hovering = true;
        // reset timeout
        startTimeout();
    })
        .on("mouseleave", function () {
        // The timeout is needed incase you go back to the main menu
        resetHover();
    });

    function startTimeout() {
        // This method gives you 1 second to get your mouse to the sub-menu
        timeout = setTimeout(function () {
            closeMenu();
        }, 1000);
    };

    function closeMenu() {
        // Only close if not hovering
        if (!hovering) {
            $('.dropdown-tool-menu').stop(true, true).slideUp(400);
        }
    };

    function resetHover() {
        // Allow the menu to close if the flag isn't set by another event
        hovering = false;
        // Set the timeout
        startTimeout();
    };
    
    // slide the Text
    
    function metaHomeText ($h,$s) {
    $($h).hover(
        function () {
            $($s).stop(true, true).show();
        },
        function () {
            $($s).stop(true, true).hide();
        }
    );
    };
    if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ){
        metaHomeText($('#meta-cal'),$('#meta-cal-text'));
        metaHomeText($('#meta-nl'),$('#meta-nl-text'));
        metaHomeText($('#meta-loc'),$('#meta-loc-text'));
    }
    
    
    
});
 
// detect ie version    
function getInternetExplorerVersion()
{
  var rv = -1;
  if (navigator.appName == 'Microsoft Internet Explorer')
  {
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );
  }
  else if (navigator.appName == 'Netscape')
  {
    var ua = navigator.userAgent;
    var re  = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );
  }
  return rv;
}
   // alert (getInternetExplorerVersion());
;})(jQuery);
